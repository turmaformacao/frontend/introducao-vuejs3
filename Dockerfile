FROM node:16

WORKDIR /app
ADD . /app
RUN npm install --force
RUN npm run build
RUN npm install -g serve 
EXPOSE 80

ENTRYPOINT [ "serve", "-s", "dist", "-l", "80" ]