const nomeSessao = 'loja'

const criarSessao = (sessao) => {
    console.log('Sessão de usuário criada com sucesso!')
    localStorage.setItem(nomeSessao, JSON.stringify(sessao))
}

const recuperaSessao = () => {
   return JSON.parse(localStorage.getItem(nomeSessao))
}

const recuperaSessaoToken = () => {
   let sessao = recuperaSessao()
   return sessao ? sessao.access_token: ''
}

const removeSessao = () => {
    console.log('Sessão de usuário removida com sucesso!')
    return localStorage.removeItem(nomeSessao)
}

export default {
    criarSessao,
    recuperaSessao,
    removeSessao,
    recuperaSessaoToken
}
