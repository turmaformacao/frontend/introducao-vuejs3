const converteValorInteiroMoeda = (valor) => {
    return new Intl.NumberFormat('de-DE').format(valor)
}

export {
    converteValorInteiroMoeda
}