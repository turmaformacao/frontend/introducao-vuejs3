import axios from 'axios'
import router from '../../router'
import sessao from '../sessao/'

const instance = axios.create({
    headers: {'Authorization': `Bearer ${sessao.recuperaSessaoToken()}`}
  })

  const requestPOST = (url, payload = null) => {
      return instance.post(url, payload)
        .then(response => response)
        .catch(e => {
          if (e.response.status === 401) {
            router.push({name: 'login'})
          }
        })
  }

  const requestGET = (url, params = null) => {
    return instance.get(url, {params})
      .then(response => response)
}

  export {
      requestPOST,
      requestGET
  }
