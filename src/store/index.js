import { reactive } from "@vue/reactivity"

const getState = reactive ({
    resultadoPesquisaProduto: '',
    itensAdicionadosCarrinho: [],
    valorTotalProdutoCarrinho: Number,
    env: {
        VUE_APP_URL_API_LOJA: 'http://localhost:8000/api'
    },
    notificacao: {
        show: false,
        mensagem: null
    }
})

const setState = (propriedade, valor)  => {
    getState[propriedade] = valor
}


const setNotificacao = (obj) => {
    getState.notificacao = obj

    setTimeout(() => {
        getState.notificacao.show = false
        getState.notificacao.mensagem = null
    }, 3000)
}

const setItensCarrinho = (produto)  => {
    getState.itensAdicionadosCarrinho.push(produto)
}

const removeItensCarrinho = (indice) => {
    getState.itensAdicionadosCarrinho.splice(indice, 1)
}

const setValorTotalProdutoCarrinho = (valorTotal) => {
    getState.valorTotalProdutoCarrinho = valorTotal
}

export {
    getState,
    setState,
    setItensCarrinho,
    removeItensCarrinho,
    setValorTotalProdutoCarrinho,
    setNotificacao
}
